<?php

namespace Drupal\Tests\lightning_api\Functional;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests Lightning API's database update path.
 *
 * @group lightning_api
 * @group lightning
 */
class UpdatePathTest extends UpdatePathTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    if (str_starts_with(\Drupal::VERSION, '10.')) {
      $this->databaseDumpFiles = [
        __DIR__ . '/../../fixtures/drupal-9.4.0-update-from-1.0.0-rc1.php.gz',
      ];
    }
    else {
      $this->databaseDumpFiles = [
        __DIR__ . '/../../fixtures/drupal-8.8.0-update-from-1.0.0-rc1.php.gz',
      ];
    }
  }

  /**
   * Tests Lightning API's database update path.
   */
  public function testUpdatePath() {
    $this->config('core.extension')
      ->clear('module.openapi_redoc')
      ->save();

    $this->container->get('keyvalue')
      ->get('system.schema')
      ->delete('openapi_redoc');

    $this->config('entity_browser.browser.media_browser')->delete();
    $this->config('media.type.tweet')->delete();

    $this->runUpdates();
    $this->drush('update:lightning', [], ['yes' => NULL]);
  }

}
